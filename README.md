# Overview

Repository includes the different steps to complete "DevOps Challenge" for Company A.


## Summary

### Mandatory

#### Application

- [x] Web Application Framework deployed
- [x] Database deployed
- [ ] Web application and Database defined using Helm Chart
- [x] Web application and Database defined using Docker Compose
- [x] "Hello World" page showing communication with DB

#### Infrasctucture

- [x] Cloud Kubernetes Provider - AWS
- [x] Kubernetes Cluster - AWS Fargate
- [x] Configuration is defined in Terraform

#### CI/CD

- [x] Application and Terraform codes are managed on Gitlab
- [x] Automatically deployed after merging request into main is approved

### Additional

- [x] E2E Solution architecture for easy understanding of the solution
- [x] AWS-vault used to avoid plain-text credentials.
- [x] Both Infrastructure and CI/CD use Minimum Privilege principle, to allow Pipeline executor only the required permissions to apply changes in different environments 
- [x] Environment Specific resources (dev, staging, production)
- [x] Best practices on Terraform design (variables, main, etc)
- [x] HTTPS Get Lock enabled to avoid concurrent terraform Apply requests
- [x] CI/CD pipeline uses variables to avoid sensitive information in the sourceCode

### Future Improvements

- [] Replace docker-compose to helm chart versioning for applications
- [] Add a Application Load Balancer in front of Kubernetes cluster for scalability
- [] Add a DNS solution to improve End-user experience
- [] Try other Public Cloud providers to analyze cost impact


## Solution

![DeVOps Challenge](devops_challenge.png "E2E Architecture")

### General Workflow

[1] Git users push a new change into one of the features/main branch
[2] Gitlab CI/CD pipeline creates the artifacts and pushes the changes into the different defined stages: Test and Lint, Build and Push, Staging Plan, Staging Apply, Production Plan, Production Apply, Destroy
[3] Applied changes are inmediately available once merged into Production


### Pre-conditions

Before running this repository, the follow steps should be follow to setup the environment

+ In the CI/CD Pipeline, the following variables must be set in advance: AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, ECR_REPO, TF_VAR_db_password, TF_VAR_db_username, TF_VAR_django_secret_key
+ AWS-Vault must be downloaded and AWS IAM user added. After this, we need to run the following command to grant temporary credentials to AWS user in order to run docker-compose files.
  `aws-vault exec AWS_USER_NAME --duration=12h`


### Execution Commands

+ In case you want to init Terraform using S3 as artifacts repository and DynamoDB for HTTPS Get lock
  `docker-compose -f deploy/docker-compose.yml run --rm terraform init`
+ In case you want to verify terraform file syntaxis, please use the following command:
  `docker-compose -f deploy/docker-compose.yml run --rm terraform fmt`
+ In case you want to verify the resources terraform is going to create
  `docker-compose -f deploy/docker-compose.yml run --rm terraform plan`
+ In case you want to apply the resources into AWS using Terraform
  `docker-compose -f deploy/docker-compose.yml run --rm terraform apply`


### Final Notes

After completing the pre-conditions, the following commands will allow you to push any change into the different stages

+ `git add .`
+ `git commit -am "Meaningful comment"`
+ `git push --set-upstream origin feature/ecs-cluster`